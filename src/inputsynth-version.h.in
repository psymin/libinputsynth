/* inputsynth-version.h.in
 *
 * Copyright 2018 Christoph Haag
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization.
 */

#ifndef INPUT_SYNTH_VERSION_H
#define INPUT_SYNTH_VERSION_H

#if !defined(INPUT_SYNTH_INSIDE) && !defined(INPUT_SYNTH_COMPILATION)
# error "Only <inputsynth.h> can be included directly."
#endif

/**
 * SECTION:inputsynth-version
 * @short_description: inputsynth version checking
 *
 * inputsynth provides macros to check the version of the library
 * at compile-time
 */

/**
 * INPUT_SYNTH_MAJOR_VERSION:
 *
 * inputsynth major version component (e.g. 1 if %INPUT_SYNTH_VERSION is 1.2.3)
 */
#define INPUT_SYNTH_MAJOR_VERSION (@MAJOR_VERSION@)

/**
 * INPUT_SYNTH_MINOR_VERSION:
 *
 * inputsynth minor version component (e.g. 2 if %INPUT_SYNTH_VERSION is 1.2.3)
 */
#define INPUT_SYNTH_MINOR_VERSION (@MINOR_VERSION@)

/**
 * INPUT_SYNTH_MICRO_VERSION:
 *
 * inputsynth micro version component (e.g. 3 if %INPUT_SYNTH_VERSION is 1.2.3)
 */
#define INPUT_SYNTH_MICRO_VERSION (@MICRO_VERSION@)

/**
 * INPUT_SYNTH_VERSION
 *
 * inputsynth version.
 */
#define INPUT_SYNTH_VERSION (@VERSION@)

/**
 * INPUT_SYNTH_VERSION_S:
 *
 * inputsynth version, encoded as a string, useful for printing and
 * concatenation.
 */
#define INPUT_SYNTH_VERSION_S "@VERSION@"

#define INPUT_SYNTH_ENCODE_VERSION(major,minor,micro) \
        ((major) << 24 | (minor) << 16 | (micro) << 8)

/**
 * INPUT_SYNTH_VERSION_HEX:
 *
 * inputsynth version, encoded as an hexadecimal number, useful for
 * integer comparisons.
 */
#define INPUT_SYNTH_VERSION_HEX \
        (INPUT_SYNTH_ENCODE_VERSION (INPUT_SYNTH_MAJOR_VERSION, INPUT_SYNTH_MINOR_VERSION, INPUT_SYNTH_MICRO_VERSION))

/**
 * INPUT_SYNTH_CHECK_VERSION:
 * @major: required major version
 * @minor: required minor version
 * @micro: required micro version
 *
 * Compile-time version checking. Evaluates to %TRUE if the version
 * of inputsynth is greater than the required one.
 */
#define INPUT_SYNTH_CHECK_VERSION(major,minor,micro)   \
        (INPUT_SYNTH_MAJOR_VERSION > (major) || \
         (INPUT_SYNTH_MAJOR_VERSION == (major) && INPUT_SYNTH_MINOR_VERSION > (minor)) || \
         (INPUT_SYNTH_MAJOR_VERSION == (major) && INPUT_SYNTH_MINOR_VERSION == (minor) && \
          INPUT_SYNTH_MICRO_VERSION >= (micro)))

#endif /* INPUT_SYNTH_VERSION_H */

