/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef __INPUT_SYNTH_H__
#define __INPUT_SYNTH_H__

#include <glib-object.h>
#include <gmodule.h>

G_BEGIN_DECLS

#define INPUT_TYPE_SYNTH input_synth_get_type()
G_DECLARE_DERIVABLE_TYPE (InputSynth, input_synth, INPUT, SYNTH, GObject)

struct _InputSynthClass
{
  GObjectClass parent_class;
  void (*move_cursor) (InputSynth *self, int x, int y);
  void (*click) (InputSynth *self, int x, int y, int button, gboolean press);
  void (*character) (InputSynth *self, char c);
  GString * (*get_backend_name) (InputSynth *self);
};

/**
 * InputsynthBackend:
 * @INPUTSYNTH_BACKEND_XI2: Use XTestFake events to synthesize input.
 * Optionally create a second mouse cursor with xinput2.
 * @INPUTSYNTH_BACKEND_XDO: Use libxdo from xdotools.
 * @INPUTSYNTH_BACKEND_WAYLAND_CLUTTER: Use Clutter's remote input API to
 * synthesize input on gnome wayland.
 *
 * Type of backend to use.
 *
 */
typedef enum {
  INPUTSYNTH_BACKEND_XI2,
  INPUTSYNTH_BACKEND_XDO,
  INPUTSYNTH_BACKEND_WAYLAND_CLUTTER
} InputsynthBackend;

InputSynth *input_synth_new (InputsynthBackend backend);

void
input_synth_move_cursor (InputSynth *self, int x, int y);

void
input_synth_click (InputSynth *self, int x, int y, int button, gboolean press);

void
input_synth_character (InputSynth *self, char c);

GString *
input_synth_get_backend_name (InputSynth *self);

G_END_DECLS

#endif /* __INPUT_SYNTH_H__ */
