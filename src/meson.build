so_version = 0
api_version = '0.13'
api_path = 'libinputsynth-' + api_version

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
  input: 'inputsynth-version.h.in',
  output: 'inputsynth-version.h',
  configuration: version_conf,
  install_dir: join_paths(get_option('includedir'), api_path)
)

inputsynth_inc = include_directories('.')

pkg = import('pkgconfig')

inputsynth_deps = [
  gio_dep,
  glib_dep,
  gmodule_dep
]

inputsynth_sources = [
  'inputsynth.c'
]

inputsynth_headers = [
  'inputsynth.h'
]
install_headers(inputsynth_headers, subdir: api_path)

args = [
  '-DPLUGIN_DIR="@0@"'.format(join_paths(get_option('prefix'),
                                         get_option('libdir'),
                                         'libinputsynth-' + api_version + '/plugins')),
  '-DAPI_VERSION="' + api_version +'"'
]


inputsynth_lib = shared_library('inputsynth-' + api_version,
  inputsynth_sources,
  version: meson.project_version(),
  soversion: so_version,
  dependencies: inputsynth_deps,
  install: true,
  c_args : args
)

pkg.generate(
  description: 'A set of shared libraries for synthesizing desktop input on X11 and wayland (main)',
    libraries: inputsynth_lib,
         name: 'inputsynth',
     filebase: api_path,
      version: meson.project_version(),
      subdirs: api_path,
     requires: 'glib-2.0',
  install_dir: join_paths(get_option('libdir'), 'pkgconfig')
)

if xdo_lib.found()
  inputsynth_sources_xdo = [
    'inputsynth-xdo.c',
  ]

  inputsynth_headers_xdo = [
    'inputsynth-xdo.h'
  ]

  inputsynth_xdo_deps = [
    xdo_lib
  ]

  inputsynth_lib_xdo = shared_module('inputsynth_xdo',
    inputsynth_sources_xdo,
    dependencies: [ inputsynth_deps, inputsynth_xdo_deps ],
    link_with: inputsynth_lib,
    install_dir: join_paths(get_option('libdir'), 'libinputsynth-' + api_version + '/plugins'),
    install: true,
  )
endif

if x11_dep.found() and xtst_dep.found() and xi_dep.found()
  inputsynth_sources_xi2 = [
    'inputsynth-xi2.c',
  ]

  inputsynth_headers_xi2 = [
    'inputsynth-xi2.h'
  ]

  inputsynth_xi2_deps = [
    x11_dep,
    xtst_dep,
    xi_dep
  ]

  inputsynth_lib_xi2 = shared_module('inputsynth_xi2',
    inputsynth_sources_xi2,
    dependencies: [ inputsynth_deps, inputsynth_xi2_deps ],
    link_with: inputsynth_lib,
    install_dir: join_paths(get_option('libdir'), 'libinputsynth-' + api_version + '/plugins'),
    install: true,
  )
endif

if libmutter_dep.found() and mutter_clutter_dep.found()
  inputsynth_sources_wayland_clutter = [
    'inputsynth-wayland-clutter.c',
  ]

  inputsynth_headers_wayland_clutter = [
    'inputsynth-wayland-clutter.h'
  ]

  inputsynth_wayland_clutter_deps = [
    mutter_clutter_dep
  ]

  inputsynth_lib_wayland_clutter = shared_module('inputsynth_wayland_clutter',
    inputsynth_sources_wayland_clutter,
    dependencies: [ inputsynth_deps, inputsynth_wayland_clutter_deps ],
    link_with: inputsynth_lib,
    install_dir: join_paths(get_option('libdir'), 'libinputsynth-' + api_version + '/plugins'),
    install: true,
    build_rpath : libmutter_dep.get_pkgconfig_variable('typelibdir'),
    install_rpath : libmutter_dep.get_pkgconfig_variable('typelibdir')
  )
endif

inputsynth_dep = declare_dependency(
  sources: [],
  link_with: inputsynth_lib,
  include_directories: [ inputsynth_inc ],
  dependencies: inputsynth_deps,
)

if get_option('introspection')
  inputsynth_gir = gnome.generate_gir(
    inputsynth_lib,
    sources: inputsynth_sources + inputsynth_headers,
    namespace: 'libinputsynth',
    nsversion: api_version,
    identifier_prefix: 'libinputsynth',
    symbol_prefix: 'libinputsynth',
    export_packages: api_path,
    includes: [ ],
    header: 'libinputsynth/inputsynth.h',
    install: true,
  )
endif

